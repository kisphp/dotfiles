import * as cdk from 'aws-cdk-lib';
import * as eks from 'aws-cdk-lib/aws-eks';
import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as iam from 'aws-cdk-lib/aws-iam';


export class VpcStack extends cdk.Stack {
    // public readonly vpc: ec2.Vpc;

    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        // this.vpc = new ec2.Vpc(this, 'EksVpc', {
        //     maxAzs: 2,
        //     natGateways: 1,
        //     subnetConfiguration: [
        //         {
        //             cidrMask: 24,
        //             name: 'Public',
        //             subnetType: ec2.SubnetType.PUBLIC,
        //         },
        //         {
        //             cidrMask: 24,
        //             name: 'Private',
        //             subnetType: ec2.SubnetType.PRIVATE_WITH_EGRESS,
        //         },
        //     ]
        // });
        //
        // new cdk.CfnOutput(this, 'VpcId', {
        //     value: this.vpc.vpcId,
        // });
    }
}
