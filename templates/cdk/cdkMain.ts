#!/usr/bin/env node

import * as cdk from 'aws-cdk-lib';
import { VpcStack } from '../lib/vpc-stack';
import { EksClusterStack } from "../lib/eks-stack";

const app = new cdk.App();
const env = {
    // account: "73033EXAMPLE",
    // region: "us-east-1",
    account: process.env.CDK_DEFAULT_ACCOUNT,
    region: process.env.CDK_DEFAULT_REGION
};
const vpcStack = new VpcStack(app, 'VpcStack', { env });
const eksClusterStack = new EksClusterStack(app, 'EksClusterStack', {
    vpc: vpcStack.vpc,
    env
});
eksClusterStack.addDependency(vpcStack);
