variable "vpc_name" {
  type = string
  description = "(Required) The name of the VPC"
}

variable "subnet_public_name" {
  type = string
  default = ""
}

variable "subnet_private_name" {
  type = string
  default = ""
}

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_name]
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Name"
    values = [length(var.subnet_public_name) > 0 ? var.subnet_public_name : "${var.vpc_name}-public-*"]
  }
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Name"
    values = [length(var.subnet_private_name) > 0 ? var.subnet_private_name : "${var.vpc_name}-private-*"]
  }
}
