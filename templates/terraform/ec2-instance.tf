variable "instance_type" {
  type = string
  default = "t2.micro"
  description = "(Optional) EC2 instance type"
}

resource "aws_instance" "ec2-public" {
  ami = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"

  associate_public_ip_address = true

  subnet_id = sort(data.aws_subnets.public[0].ids)[0]

  vpc_security_group_ids = [
    aws_security_group.ec2.id
  ]

  key_name = var.key-pair-name

  #  iam_instance_profile = aws_iam_instance_profile.ssm.id

  root_block_device {
    volume_size = 8
    delete_on_termination = true
  }

  user_data = <<EOF
#!/bin/bash

# This will run as root
# PUBLIC_IP=$$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
# echo "EC2 instance ready on public ip: $${PUBLIC_IP}" > /home/ubuntu/startup.log

apt-get update
apt-get install -y vim mc tree jq ncdu wget curl

EOF

  tags = {
    Name = var.ec2_name
  }
}

output "public_ip" {
  value = aws_instance.ec2-public.public_ip
}
