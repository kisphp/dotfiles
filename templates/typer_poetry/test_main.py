from typer.testing import CliRunner
from __APP_NAME__.__main__ import app


def test_main_no_args():
    runner = CliRunner()
    runner.invoke(app, [])

def test_main_with_args():
    runner = CliRunner()
    runner.invoke(app, [])

def test_main_pass_string():
    runner = CliRunner()
    runner.invoke(app, [
        'QUERY-123'
    ])

# def test_main_only_source_file():
#     runner = CliRunner()
#     runner.invoke(app, [
#         '-s', './kube/old/old-config-map.yaml',
#     ])
#
# def test_main_only_target():
#     runner = CliRunner()
#     runner.invoke(app, [
#         '-t', './_tmp/old'
#     ])
