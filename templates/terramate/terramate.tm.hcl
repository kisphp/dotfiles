terramate {
  config {

  }
}

globals {
  aws_account_id = "112233445566"
  region = "us-east-1"
  profile = "la"
}

// Configure default Terraform version and default providers
globals "terraform" {
  version = "~> 1.0"
}

# Will be added to all stacks
globals "terraform" "required_providers" "aws" {
  source  = "hashicorp/aws"
  version = ">= 5.0"
  enabled = true
}

globals "terraform" "backend" "s3" {
  enabled = true
  bucket_name = "terraform-state-${global.aws_account_id}"
  dynamodb_table = "terraform-state-lock"
}

import {
  source = "./tm_helpers/generate_provider.tm.hcl"
}
import {
  source = "./tm_helpers/generate_backend.tm.hcl"
}
