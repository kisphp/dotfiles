generate_hcl "_generated_backend.tf" {
  condition = tm_try(global.terraform.backend.s3.enabled, true)

  content {
    terraform {
      backend "s3" {
        region         = "us-east-1"
        bucket         = global.terraform.backend.s3.bucket_name
        key            = "${terramate.stack.path.relative}/terraform.tfstate"
        encrypt        = true
        dynamodb_table = global.terraform.backend.s3.dynamodb_table
      }
    }
  }
}
