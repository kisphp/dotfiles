#!/usr/bin/env python3

class CliTable:
    def __init__(self):
        self.payload = []
        self.lengths = {}
        self.registry = {}
        self.pricing = {}

    def _read_data(self, data, key):
        if key not in self.lengths:
            self.lengths[key] = len(key)

        self.lengths[key] = max(len(str(data[key])), self.lengths[key])

        return str(data[key])

    def show(self):
        header = False
        header_row = ''
        for item in self.payload:
            if not header:
                for hk in item:
                    item_length = self.lengths[hk]
                    header_row += f"| {hk.ljust(item_length)} "
                header_row += " |"

                print('=' * len(header_row))
                print(header_row)
                print('-' * len(header_row))

                header = True
            for key in item:
                print(f"| {item[key].ljust(self.lengths[key])} ", end='')
            print(" |")
        print('=' * len(header_row))

    @staticmethod
    def calculate_price(instance_count, instance_type, instance_lifecycle):
        pricing = {
            # https://aws.amazon.com/ec2/pricing/on-demand/
            'normal': {
                'r6gd.xlarge': 0.2304,
                't2.medium': 0.0464,
                't3.medium': 0.0416,
                't3a.medium': 0.0376,
                't4g.micro': 0.0084,
                't4g.medium': 0.0336,
                'x2gd.medium': 0.0835,
                'x2gd.large': 0.167,
                'x2gd.xlarge': 0.334,
                'x2gd.2xlarge': 0.668,
                'r6gd.2xlarge': 0.4608,
                'r6a.4xlarge': 0.9072,
                'r5ad.2xlarge': 0.524,
                'r5.2xlarge': 0.504,
                'm7i.large': 0.1008,
                'm6a.xlarge': 0.1728,
                'm6a.4xlarge': 0.6912,
                'm5a.xlarge': 0.172,
                'i4i.xlarge': 0.343,
                'i3.xlarge': 0.312,
                'i3.large': 0.156,
                'i3.2xlarge': 0.624,
                'c6i.xlarge': 0.17,
                'c6i.4xlarge': 0.68,
                'c6i.2xlarge': 0.34,
                'c6a.xlarge': 0.153,
                'c6a.4xlarge': 0.612,
                'c6a.2xlarge': 0.306,
                'c5n.large': 0.108,
                't3.large': 0.0832,
                'r6i.xlarge': 0.252,
                'r6a.xlarge': 0.2268,
                'm6a.2xlarge': 0.3456,
                'c5a.large': 0.077,
                'c5a.2xlarge': 0.308,
                'c5.2xlarge': 0.34,
                'm6a.large': 0.0864,
                't4g.nano': 0.0042,
                't2.nano': 0.0058,
                't2.micro': 0.0116,
                't2.large': 0.0928,
                'r6a.2xlarge': 0.4536,
                'm4.large': 0.1,
                'm4.4xlarge': 0.8,
                'i3en.3xlarge': 1.356,
                'm6i.4xlarge': 0.768,
                'm7gd.4xlarge': 0.8543,
            },
            # https://aws.amazon.com/ec2/spot/pricing/
            'spot': {
                't4g.micro': 0.0038,
                't4g.2xlarge': 0.1186,
                'r6in.2xlarge': 0.3023,
                'r6idn.2xlarge': 0.3381,
                'r5.2xlarge': 0.2374,
                'r4.2xlarge': 0.238,
                'm6g.2xlarge': 0.1365,
                'm6a.2xlarge': 0.1957,
                'm5zn.2xlarge': 0.3567,
                'm5n.2xlarge': 0.217,
                'm5ad.2xlarge': 0.2811,
                'm5a.2xlarge': 0.1851,
                'm5.4xlarge': 0.3083,
                'm4.2xlarge': 0.1854,
                'c7g.2xlarge': 0.1183,
                'c6gn.2xlarge': 0.1424,
                'c6g.2xlarge': 0.1123,
                'r6i.2xlarge': 0.2045,
                'r6g.2xlarge': 0.1802,
                'r6a.2xlarge': 0.2249,
                'r5n.2xlarge': 0.277,
                'r5d.2xlarge': 0.3233,
                'r5b.2xlarge': 0.3727,
                'r5ad.2xlarge': 0.2895,
                'm6i.2xlarge': 0.149,
                'r5dn.2xlarge': 0.3363,
            }
        }

        if instance_lifecycle in pricing:
            if instance_type in pricing[instance_lifecycle]:
                return pricing[instance_lifecycle][instance_type] * 750 * instance_count


        print(f'{instance_type} @ {instance_lifecycle} missing price')

        return 0

    def show_pricing(self):
        total_price = 0

        for item in sorted(self.pricing):
            key = item.split(':')
            counting = str(self.pricing[item]).rjust(4)
            ec2_type = key[0].ljust(20)
            ec2_price = self.calculate_price(self.pricing[item], key[0], key[1])

            total_price += ec2_price
            show_price = f'{ec2_price:0,.2f}'

            print(f'| {counting} | {ec2_type} | {show_price.rjust(12)} $ |')

        print(f'Total price: {total_price:0,.2f} $')
