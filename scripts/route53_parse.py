#!/usr/bin/env python3

import json
from libs import CliTable


class Route53Parser(CliTable):
    def add(self, data):
        self.payload.append({
            'Id': self._read_data(data, 'Id'),
            'Name': self._read_data(data, 'Name'),
            'ResourceRecordSetCount': self._read_data(data, 'ResourceRecordSetCount'),
        })


if __name__ == '__main__':
    with open('/tmp/route53.json', 'r') as file:
        dict = json.loads(file.read())

    td = Route53Parser()
    for zone in dict['HostedZones']:
        td.add(zone)

    td.show()
