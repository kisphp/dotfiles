#!/usr/bin/env python3

import sys


class FileContent:
    def __init__(self):
        self.lines = []

    def add(self, line):
        self.lines.append(line.strip("\n"))

    def is_empty(self):
        return len(self.lines) < 1

    def get_content(self):
        return "\n".join(self.lines)


class ManifestSplitter:
    def __init__(self):
        self.files = []
        self.type_indexes = {}

    def write_file(self, filename, content, append=False):
        if 'kind' not in content:
            print(f'File {filename}.yaml is not a valid k8s manifest')
            return None
        print(f'Writing file: {filename}.yaml')
        with open(f'{filename}.yaml', 'a' if append else 'w') as fh:
            fh.write(content + "\n")

    def traverse_files(self):
        for idx, file in enumerate(self.files):
            file_content = file.get_content()
            start = file_content.find('kind')
            end = file_content.find("\n", start)

            kind_line = file_content[start:end]
            space = kind_line.find(":")

            kind = kind_line[space+1:].strip().lower()

            filename = kind
            if self.type_indexes.get(kind):
                filename = f"{kind}-{len(self.type_indexes.get(kind))}"
                self.type_indexes[kind].append(1)
            else:
                self.type_indexes.update({kind: [1]})

            self.write_file(filename, file_content)

    def add_file(self, file: FileContent):
        if not file.is_empty():
            self.files.append(file)

    def parse(self, source_file):
        with open(source_file) as file:
            lines = file.readlines()
            file = FileContent()
            for line in lines:
                if line.strip() == '---':
                    self.add_file(file)
                    file = FileContent()
                else:
                    file.add(line.strip("\n"))

            self.add_file(file)

        self.traverse_files()


if __name__ == '__main__':
    source_file = sys.argv[1]
    ms = ManifestSplitter()
    ms.parse(source_file)
