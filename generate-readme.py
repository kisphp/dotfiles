#!/usr/bin/env python3

import os
import re

legend = {}

for root, dirs, files in os.walk('./plugins'):
    for file in files:
        if '_plugin' in file:
            continue;

        with open(os.path.join(root, file), 'r') as f:
            file_content = f.read()
            pattern = r'(\w+)\s*\(\s*\)\s*\{\s*#\s*(.*)'

            # Find all matches in the Bash content
            matches = re.findall(pattern, file_content)

            # Print the matched function names and descriptions
            for match in matches:
                function_name, description = match
                print(f"Function Name: {function_name}, Description: {description}")
                if function_name not in legend:
                    legend[function_name] = {
                        'cmd_name': f'`{function_name}`',
                        'cmd_description': f'`{description}`',
                        'cmd_file': file,
                    }
                else:
                    print(f"Function Name: {function_name} already exists")
                    exit(0)

            # Define the regex pattern for Bash aliases
            alias_pattern = r"alias\s+(\w+)\s*=\s*'([^'\"\|]*)'"

            # Find all matches for the alias pattern
            matches = re.findall(alias_pattern, file_content)

            # Populate the aliases dictionary
            for alias_name, command in matches:
                if alias_name not in legend:
                    legend[alias_name] = {
                        'cmd_name': alias_name,
                        'cmd_description': f'`{command}`',
                        'cmd_file': file,
                    }
                else:
                    print(f"Alias Name: {alias_name} already exists")
                    exit(0)

# Sort dictionary by keys
sorted_dict = {key: legend[key] for key in sorted(legend.keys())}

legend_output = [
    '| Function Name | Description | Source File |',
    '| --- | --- | --- |',
]
for item in sorted_dict.items():
    print(f"Function Name: {item[1]['cmd_name']}, Description: {item[1]['cmd_description']}")
    legend_output.append(f'| {item[1]['cmd_name']} | {item[1]['cmd_description']} | {item[1]['cmd_file']} |')

with open('LEGEND.md', 'w') as f:
    f.writelines("\n".join(legend_output))
