# KISPHP Dotfiles

My bash aliases and functions to help working with CLI

## Install

Optional preinstallation requirements for ubuntu/debian
```bash
sudo apt-get update
sudo apt-get install -y bc zip unzip curl wget sudo git
```

To install this tool just execute this line on your terminal. Installation directory will be `~/.dotfiles`
```bash
curl https://gitlab.com/kisphp/dotfiles/raw/main/install.sh | bash -
```

If you have the tool installed from the old gitlab repository run the following commands on your terminal:

```shell
cd ~/.dotfiles/
git remote set-url origin https://gitlab.com/kisphp/dotfiles.git
git pull
git checkout main
git pull --rebase
```

> [For manual installation click here](INSTALL.md)


## Full Aliases list

For full aliases and functions list, please [click here](./LEGEND.md)


## Configuration `~/.dotfiles.cfg`

| Parameter       | Default Value | Info                                              |
|-----------------|--------------:|---------------------------------------------------|
| KP_DEBUG        |             0 | enable debug messages                             |
| KP_LOGS         |             0 | keep a log with all commands executed by the tool |
| KP_COLORS       |             1 | enable colored text                               |
| KP_UPGRADE_DAYS |        432000 | check for upgrade at each 5 days                  |

## Available Aliases

### Dotfiles

| Command                     | Details                                                                                                                                                        |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| _upgrade_dotfiles           | force update of the tool                                                                                                                                       |
| _upgrade_dotfiles_timestamp | update last check lock file                                                                                                                                    |
| _should_upgrade             | check for new upgrades                                                                                                                                         |
| allow_ssh_auth              | `allow_ssh_auth user@server.domain` will allow you to connect via SSH without password. For a different port to ssh, just append `-p <port_number>` at the end |
| banip                       | ban ip address                                                                                                                                                 |
| dirsize                     | count size of all files inside a directory                                                                                                                     |
| errorText                   | Text on red background                                                                                                                                         |
| infoText                    | Text on yello background                                                                                                                                       |
| successText                 | Text on green background                                                                                                                                       |
| genpass [int: chars]        | generate random scring at size                                                                                                                                 |
| gitci                       | generate gitlab merge request description template                                                                                                             |
| sshls                       | list public keys md5 fingerprints                                                                                                                              |
| ken64                       | Base64 encoding without new line                                                                                                                               |
| kdec64                      | Base64 decoding without new line                                                                                                                               |
| mcd                         | create directory and get inside it                                                                                                                             |
| o                           | Open file in default app, open url in browser from terminal                                                                                                    |
| p2                          | run python 2                                                                                                                                                   |
| p3                          | run python 3                                                                                                                                                   |
| rmme                        | runs `rm -rf $(pwd) && cd ../`                                                                                                                                 |
| showlogs                    | show continuous updates in system log files from /var/logs. It can receive a path as firts parameter `showlogs /path/`                                         |
| vim_config                  | Run VIM configuration script                                                                                                                                   |

### Server management

| Command    | Details                                                                                    |
|------------|--------------------------------------------------------------------------------------------|
| apare      | run `sudo /etc/init.d/apache2 restart`                                                     |
| clean_repo | alias to `git_clean_repo`                                                                  |
| elre       | Restart Elasticsearch                                                                      |
| eltest     | Check elasticsearch response with curl. Usage `eltest` or specify the port: `eltest 10009` |
| myre       | run `sudo /etc/init.d/mysql restart`                                                       |
| ngre       | run `sudo /etc/init.d/nginx restart`                                                       |
| phpre      | run `sudo /etc/init.d/php7-fpm restart`                                                    |

### GIT

| Command                  | Details                                                                                                       |
|--------------------------|---------------------------------------------------------------------------------------------------------------|
| clean_repo               | alias to `git_clean_repo`                                                                                     |
| [cln](docs/cln.md)       | clone a repository from Github                                                                                |
| edited                   | display edited files in repository                                                                            |
| gs2h                     | convert git remote from SSH to HTTPS                                                                          |
| gh2s                     | convert git remote from HTTPS to SSH                                                                          |
| ged                      | alias to `edited`                                                                                             |
| gign                     | add file or directory to gitignore                                                                            |
| git_config               | Run git configuration script                                                                                  |
| git_clean_repo           | remove merged branched from local and remote                                                                  |
| [makeup](docs/makeup.md) | faster commit and push to git repositorygst                                                                   |
| mergefile                | `mergefile original_file file_to_merge_in_original` get content from second file and merge it with first file |
| uncommit                 | Cancel last commit (if it was not pushed)                                                                     |

### PHP

| Command                | Details                                                            |
|------------------------|--------------------------------------------------------------------|
| codecept               | run codeception suite                                              |
| [debug](docs/debug.md) | configure xdebug parameters for php scripts                        |
| php_project            | create a php project in current directory                          |
| php2md                 | generate DOCS.md file from the current directory                   |
| pfix                   | run `php-cs-fixer fix v` command                                   |
| sf                     | shortcut to `app/console` or `bin/console` or `vendor/bin/console` |

### Docker

| Command                  | Details                                                            |
|--------------------------|--------------------------------------------------------------------|
| dcon                     | enter in a running docker container                                |
| dcpl                     | list running docker processes and display size                     |
| dcl                      | list running docker processes                                      |
| dcs                      | stop running docker processes                                      |
| dcr                      | delete stopped docker processes                                    |
| dcimg                    | display docker images                                              |
| dcimr                    | delete docker image                                                |
| dcim                     | display docker images                                              |
| dcv                      | list docker volumes                                                |
| [docker](docs/docker.md) | docker tools                                                       |
| docrun                   | run a command into a php container                                 |
| php_docker               | create a docker running configuration for php-fpm, nginx and mysql |
| py_docker                | create docker configuration with minimal flask application         |


### Vagrant

| Command   | Details                            |
|-----------|------------------------------------|
| kvm       | install kisphp virtual machine     |
| labelText | Text on blue background            |
| listbox   | List vagrant boxes location        |
| vagon     | run `vagrant up $@ && vagrant ssh` |
| vagof     | run `vagrant suspend`              |
| vagoff    | run `vagrant suspend && exit`      |
| vagkill   | run `vagrant destroy && exit`      |
| vagkil    | run `vagrant destroy`              |
| vagSrc    | search for VM path                 |
| vgr       | vagrant reload                     |
| vstg      | alias to `vagrant global-status`   |
| vstopr    | stop all vagrant running machines  |
| vsh       | run `vagrant ssh`                  |
| vst       | run `vagrant status`               |
| vup       | run `vagrant up`                   |
| von       | run `vaon` function                |

### Editors

| Command | Details                                                                                                |
|---------|--------------------------------------------------------------------------------------------------------|
| edit    | Open PHPStorm in specified directory. If no directory is given, then it will open in current directory |

### Python

| Command | Details                                                         |
|---------|-----------------------------------------------------------------|
| pipup   | upgrade pip packages                                            |
| pip3up  | upgrade pip3 packages                                           |
| penv    | create python3 virtual environment, activate it and upgrade pip |
| pycli   | Create a poetry and typer cli project                           |

### Kubernetes

| Command | Details                                                           |
|---------|-------------------------------------------------------------------|
| k       | call `kubectl`                                                    |
| mk      | call `minikube`                                                   |
| kwho    | call `aws sts get-caller-identity`                                |
| kal     | run `kubectl get po,svc,deploy,cronjobs,rs,cm,ing,pvc,no -o wide` |
| kname   | change kubectl context to use a namespace in all commands         |
| mkup    | Start minikube and enable ingress, registry plugins               |
| k8s | generate kubernetes manifests                                     |
| k8s ci | create `.gitlab-ci.yaml` file in current working directory |
| k8s split path/to/manifest.yaml | split k8s manifests into multiple files |


### Terraform

| Command | Details                                                                                                            |
|---------|--------------------------------------------------------------------------------------------------------------------|
| doctf   | run a specific terraform version inside a docker container                                                         |
| dtfu    | run docker pull for doctf image                                                                                    |
| tf      | call `terraform`                                                                                                   |
| tfa     | call `terraform apply`                                                                                             |
| tfcost  | Run infracost on local directory                                                                                   |
| tfg     | generate terraform files                                                                                           |
| tfp     | call `terraform plan`                                                                                              |
| tfs     | call `terraform show`                                                                                              |
| tfr     | call `terraform refresh`                                                                                           |
| tfrm    | call `terraform destroy`                                                                                           |
| tfmod   | creates files `main.tf`, `variables.tf`, `outputs.tf`, `data.tf`, `locals.tf` and `README.md` in current directory |
| tfnew   | Download specified terraform version                                                                               |
| tflink  | Set a terraform version globally                                                                                   |
| tfls    | List all avialable terraform versions downloaded in /opt/terraform/                                                |
| tgd     | Generate terragrunt graph dependencies                                                                             |
| tgdi    | Generate terragrunt graph dependencies png from dot file                                                           |


## Configuration scripts

Git Config (will require user interaction for name and email)

`scripts/git-config.sh` setup git global configuration [git-config](scripts/git-config.sh)

Vim Config

`scripts/vim-config.sh` setup vim configurations for user [vim-config](scripts/vim-config.sh)


# Make ISO images on Linux

```bash
sudo dd if=/dev/cdrom of=/path/to/cd.iso
# or
sudo dd if=/dev/dvd of=/path/to/dvd.iso
```

> ### Explanation
> `sudo` only if the user doesn't have enough permissions
> `dd` stands for Disk Dump
> `if` stands for Input File
> `of` stands for Output File

# Mounting an image

```bash
mkdir -p /mnt/isoimage
mount -o loop -t iso9660 cd.iso /mnt/isoimage
```

# Unmounting

```bash
umount -lf /mnt/isoimage
```

# Other tools to use

- *ncdu* -> view directories size and navigate through them 
