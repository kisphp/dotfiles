#!/usr/bin/env bash

showlogs () { # Show logs in a directory (`/var/log` by default)
    LOGS_PATH="$1"
    if [[ -z "${LOGS_PATH}" ]];then
        LOGS_PATH='/var/log'
    fi

    TAIL=$(which tail)
    FIND=$(which find)

    $TAIL -f $($FIND "${LOGS_PATH}" -type f -name '*.log')
}
