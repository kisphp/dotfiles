#!/usr/bin/env bash

dotinstall () {
  case "${1}" in
    kubectl|k)
      dotinstall_kubectl
      ;;
    minikube|mk)
      dotinstall_minikube
      ;;
    helm|helm3|h3)
      dotinstall_helm3
      ;;
    all)
      dotinstall_kubectl
      dotinstall_minikube
      dotinstall_helm3
      ;;
    *)
      dotinstall_help
  esac
}

dotinstall_kubectl () { # Installing kubectl on mac
  echo "Installing kubectl on mac"
  VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
  curl -sLO "https://storage.googleapis.com/kubernetes-release/release/${VERSION}/bin/darwin/amd64/kubectl"
  chmod +x kubectl
  sudo mv kubectl /usr/local/bin/kubectl
  kubectl version --client
  echo "Installed kubectl version: ${VERSION}"
  echo " "
}

dotinstall_minikube () { # Installing minikube on mac
  echo "Installing minikube on mac"
  curl -sLo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64
  chmod +x minikube
  sudo mv minikube /usr/local/bin/minikube
  minikube version
  echo "Installed minikube"
  echo " "
}

dotinstall_helm3 () { # Install helm3
  echo "Installing helm3"
  curl -s https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  helm version
  echo "Installed helm3"
}

dotinstall_help () {
  echo "Run command with one of the following options:"
  echo " "
  echo "kubectl|k -> install kubectl"
  echo "minikube|mk -> install minikube"
  echo "helm|helm3|h3 -> install helm3"
  echo " "
  echo "all -> install kubect, minikube, helm3"
  echo " "
}
