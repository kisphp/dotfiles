#!/usr/bin/env bash

GIT=$(which git)

alias clean_repo='git_clean_repo'
alias ged='edited'

cln () { # Clone a github repository
    show_manual cln $1 && return 0
    REPOSITORY=$1
    shift

    dotfiles_log "Cloned ${REPOSITORY} $*" "Git"

    if [[ -z "$*" ]];then
        $GIT clone "https://github.com/${REPOSITORY}.git"
    else
        $GIT clone "https://github.com/${REPOSITORY}.git" "$*"
    fi
    writeErrorMessage "Could not clone repository ${REPOSITORY}"
}

gitci () { # Generate Gitlab Merge Request template
  local url=$(git remote get-url origin)

  if [[ "$url" != *gitlab* ]]; then
    echo "==================================="
    echo "= This is not a gitlab repository ="
    echo "==================================="
    return 1
  fi

  mkdir -pv .gitlab/merge_request_templates/

  if [[ "${url}" == *http* ]]; then
    GIT_REPO_PATH=$(echo "$url" | awk -F/ '{print $4"/"$5}' | sed 's|\.git$||')
  else
    GIT_REPO_PATH=$(echo "$url" | awk -F: '{print $2}' | sed 's|\.git$||')
  fi

  cat <<EOF > .gitlab/merge_request_templates/Default.md
[![pipeline status](https://gitlab.com/${GIT_REPO_PATH}/badges/%{source_branch}/pipeline.svg)](https://gitlab.com/${GIT_REPO_PATH}/-/commits/%{source_branch})
[![coverage report](https://gitlab.com/${GIT_REPO_PATH}/badges/%{source_branch}/coverage.svg)](https://gitlab.com/${GIT_REPO_PATH}/-/commits/%{source_branch})
EOF
}

gitlocal () { # Create a local git repository and create a .git directory with remote to new local repo in the current directory
  PROJECT_NAME="${1}"
  PROJECT_REMOTE_PATH="${HOME}/_local_git/${PROJECT_NAME}"
  CURRENT_WORKING_DIR=$(pwd)

  if [[ ! -d "${PROJECT_REMOTE_PATH}"  ]]; then
    dotfiles_log "Create directory: ${PROJECT_REMOTE_PATH}" "Git"
    mkdir -p "${PROJECT_REMOTE_PATH}"
    dotfiles_log "Initialize remote git in directory: ${PROJECT_REMOTE_PATH}" "Git"
    GIT_DIR="${PROJECT_REMOTE_PATH}" git init --bare
  else
    dotfiles_log "Project directory ${PROJECT_REMOTE_PATH} already exists" "Git"
  fi

  git init
  dotfiles_log "Add local remote origin in: ${CURRENT_WORKING_DIR}" "Git"
  git remote add origin "${PROJECT_REMOTE_PATH}"
}

edited () { # List changed files in GIT
    BRANCH='main'

    while getopts ":b:" opt; do
        case $opt in
            b)
                shift
                if [[ "${1}" != "" ]];then
                    BRANCH="${1}"
                fi
                ;;
            *)
              echo ""
              ;;
        esac
    done

    files="$(${GIT} diff --name-only origin/${BRANCH})"

    for file in $files;
    do
        echo $file
    done
}

gign () {
    dotfiles_log "Add file '${1}' to gitignore" "Git"
    echo "${1}" >> .gitignore
}

ginit () {
    dotfiles_log "Generate .gitignore file" "Git"
    case "${1}" in
      py|python)
        echo "__pycache__" >> .gitignore
        echo "myv" >> .gitignore
        echo "venv" >> .gitignore
        echo "node_modules" >> .gitignore
        ;;

      php)
        echo "vendor" >> .gitignore
        echo "node_modules" >> .gitignore
        ;;

      *)
        echo "Usage:"
        echo ""
        echo -n "\tginit [py|python]"
        echo -n "\tginit [php]"
        echo ""
        echo ""
        ;;
    esac
}

git_clean_repo () {
    dotfiles_log "Start repo cleanup" "Git clean"
    $GIT checkout main &> /dev/null

    dotfiles_log "Run fetch" "Git clean"
    # Make sure we're working with the most up-to-date version of main.
    $GIT fetch

    # Prune obsolete remote tracking branches. These are branches that we
    # once tracked, but have since been deleted on the remote.
    dotfiles_log "Prune obsolete tracking branches" "Git clean"
    $GIT remote prune origin

    # List all the branches that have been merged fully into main, and
    # then delete them. We use the remote main here, just in case our
    # local main is out of date.
    dotfiles_log "Remove all branches that were merged" "Git clean"
    for br in $($GIT branch --merged origin/main | grep -v 'main$'); do
        dotfiles_log "Deleted branch: ${br} from ${PWD}" "Git clean"
        $GIT branch -D "${br}"
    done

    # Now the same, but including remote branches.
#    dotfiles_log "List all branches that were merged + remote" "Git clean"
#    MERGED_ON_REMOTE=$($GIT branch -r --merged origin/main | sed 's/ *origin\///' | grep -v 'main$')
#
#    if [ "$MERGED_ON_REMOTE" ]; then
#        infoText "The following remote branches are fully merged and will be removed: ${MERGED_ON_REMOTE}"
#
#        echo "Continue (y/N)? "
#        read REPLY
#        if [[ "$REPLY" == Y* ]] || [[ "$REPLY" == y* ]]; then
#            git branch -r --merged origin/main | sed 's/ *origin\///' \
#                | grep -v 'main$' | xargs -I% git push origin :% 2>&1 \
#                | grep --colour=never 'deleted'
#            successText "Remove branches were successfully deleted: ${MERGED_ON_REMOTE}"
#        fi
#    fi
}

git_config () {
    $(which bash) ~/.dotfiles/scripts/git-config.sh
}

# quick git merge and push
makeup () {
    show_manual makeup $1 && return 0

    if [[ "${1}" == "-h" ]];then
        makeup --help
        return 0
    fi

    ADD_FILES_TO_GIT=1

    if [[ -z "${1}" ]]; then
        dotfiles_log "makeup without parameters" Error
        echo "${FG_RED}You must provide a comment${NC}"
        echo "${FG_YELLOW}Usage:"
        echo "   ${0} my-comment"
        echo " "
        echo "   or to skip git add"
        echo "   ${0} -n my-comment"
        echo "${NC}"

        return 0
    fi

    while getopts ":n:a" opt; do
        case $opt in
            n)
                ADD_FILES_TO_GIT=0
                # unset $1 variable
                shift
                ;;
            *)
              ;;
        esac
    done

    if [[ "${ADD_FILES_TO_GIT}" == 1 ]];then
        ${GIT} add .
    fi

    # Get the project and ticket ID from the branch name
    project=$(git symbolic-ref --short HEAD | cut -d'-' -f1)
    ticket_id=$(git symbolic-ref --short HEAD | cut -d'-' -f2)

#    CURRENT_BRANCH_NAME=$(git branch | grep '*' | awk '{print $2}')

    # Check if the project and ticket ID are in the correct format
    if [[ "$project" =~ ^[a-zA-Z]+$ && "$ticket_id" =~ ^[0-9]+$ ]]; then
      # Prepend the project and ticket ID to the commit message
      COMMIT_MESSAGE="$project-$ticket_id ${*}"
    else
      # Skip prepending the project and ticket ID
      COMMIT_MESSAGE="${*}"
    fi

    ${GIT} commit -m "${COMMIT_MESSAGE}" && ${GIT} push -u && dotfiles_log "committed message '${COMMIT_MESSAGE}' and pushed" "Git"
}

mergefile () {
    FILE_1="${1}"
    FILE_2="${2}"

    if [[ -z $FILE_1 ]];then
        errorText "Source file not provided"
        return 1
    fi
    if [[ -z $FILE_2 ]];then
        errorText "Target file not provided"
        return 1
    fi

    # create backup file
    cp "${FILE_1}" "${FILE_1}.bkp"

    # create temporary file
    echo '' > "${FILE_1}.merged"
    # do the merge
    $GIT merge-file -p --union "${FILE_1}" "${FILE_1}.merged" "${FILE_2}" > "${FILE_1}.merged"
    # copy merge result to first file
    cp "${FILE_1}.merged" "${FILE_1}"
    # remove merge result file
    rm "${FILE_1}.merged"

    return 0
}

# Cancel last commit message
uncommit () {
    dotfiles_log "Cancel last commit" "Git"
    $GIT reset --soft HEAD^
}

gs2h () {
    infoText "Git convert from SSH to HTTPS"
    REMOTE=$(git config --get remote.origin.url)

    NEW_REMOTE=$(echo $REMOTE | sed s#\:#\/# | sed s#git\@#https\:\/\/#g)

    git remote set-url origin $NEW_REMOTE

    #git@gitlab.com:user/project.git
    #https://gitlab.com/user/project.git
}

gh2s () {
    infoText "Git convert from HTTPS to SSH"
    REMOTE=$(git config --get remote.origin.url)

    NEW_REMOTE=$(echo $REMOTE | sed s#https\:\/\/#git\@#g | sed s#\/#\:#)

    git remote set-url origin $NEW_REMOTE
}

alias ga='git add'
alias gaa='git add --all'
alias gap='git apply'
alias gapa='git add --patch'
alias gau='git add --update'
alias gb='git branch'
alias gba='git branch -a'
alias gbd='git branch -d'
alias gbda='git branch --no-color --merged | command grep -vE "^(\*|\s*(main|develop|dev)\s*$)" | command xargs -n 1 git branch -d'
alias gbl='git blame -b -w'
alias gbnm='git branch --no-merged'
alias gbr='git branch --remote'
alias gbs='git bisect'
alias gbsb='git bisect bad'
alias gbsg='git bisect good'
alias gbsr='git bisect reset'
alias gbss='git bisect start'
alias gc='git commit -v'
alias 'gc!'='git commit -v --amend'
alias gca='git commit -v -a'
alias 'gca!'='git commit -v -a --amend'
alias gcam='git commit -a -m'
alias 'gcan!'='git commit -v -a --no-edit --amend'
alias 'gcans!'='git commit -v -a -s --no-edit --amend'
alias gcb='git checkout -b'
alias gcd='git checkout develop'
alias gcf='git config --list'
alias gcl='git clone --recursive'
alias gclean='git clean -fd'
alias gcm='git checkout main'
alias gcmsg='git commit -m'
alias 'gcn!'='git commit -v --no-edit --amend'
alias gco='git checkout'
alias gcount='git shortlog -sn'
alias gcp='git cherry-pick'
alias gcpa='git cherry-pick --abort'
alias gcpc='git cherry-pick --continue'
alias gcs='git commit -S'
alias gcsm='git commit -s -m'
alias gd='git diff'
alias gdca='git diff --cached'
alias gdct='git describe --tags `git rev-list --tags --max-count=1`'
alias gdcw='git diff --cached --word-diff'
alias gdt='git diff-tree --no-commit-id --name-only -r'
alias gdw='git diff --word-diff'
alias gf='git fetch'
alias gfa='git fetch --all --prune'
alias gfo='git fetch origin'
alias gg='git gui citool'
alias gga='git gui citool --amend'
alias ggpull='git pull origin $(git_current_branch)'
alias ggpush='git push origin $(git_current_branch)'
alias ggsup='git branch --set-upstream-to=origin/$(git_current_branch)'
alias ghh='git help'
alias gignore='git update-index --assume-unchanged'
alias gignored='git ls-files -v | grep "^[[:lower:]]"'
alias git-svn-dcommit-push='git svn dcommit && git push github main:svntrunk'
alias gk='\gitk --all --branches'
alias gke='\gitk --all $(git log -g --pretty=%h)'
alias gl='git pull'
alias glg='git log --stat'
alias glgg='git log --graph'
alias glgga='git log --graph --decorate --all'
alias glgm='git log --graph --max-count=10'
alias glgp='git log --stat -p'
alias glo='git log --oneline --decorate'
alias glog='git log --oneline --decorate --graph'
alias gloga='git log --oneline --decorate --graph --all'
alias glol='git log --graph --pretty='\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit'
alias glola='git log --graph --pretty='\''%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset'\'' --abbrev-commit --all'
alias glum='git pull upstream main'
alias gm='git merge'
alias gma='git merge --abort'
alias gmom='git merge origin/main'
alias gmt='git mergetool --no-prompt'
alias gmtvim='git mergetool --no-prompt --tool=vimdiff'
alias gmum='git merge upstream/main'
alias gp='git push'
alias gpd='git push --dry-run'
alias gpoat='git push origin --all && git push origin --tags'
alias gpristine='git reset --hard && git clean -dfx'
alias gpsup='git push --set-upstream origin $(git_current_branch)'
alias gpu='git push upstream'
alias gpv='git push -v'
alias gr='git remote'
alias gra='git remote add'
alias grb='git rebase'
alias grba='git rebase --abort'
alias grbc='git rebase --continue'
alias grbi='git rebase -i'
alias grbm='git rebase main'
alias grbs='git rebase --skip'
alias grh='git reset HEAD'
alias grhh='git reset HEAD --hard'
alias grmv='git remote rename'
alias grrm='git remote remove'
alias grset='git remote set-url'
alias grt='cd $(git rev-parse --show-toplevel || echo ".")'
alias gru='git reset --'
alias grup='git remote update'
alias grv='git remote -v'
alias gsb='git status -sb'
alias gsd='git svn dcommit'
alias gsi='git submodule init'
alias gsps='git show --pretty=short --show-signature'
alias gsr='git svn rebase'
alias gss='git status -s'
alias gst='git status'
alias gsta='git stash save'
alias gstaa='git stash apply'
alias gstc='git stash clear'
alias gstd='git stash drop'
alias gstl='git stash list'
alias gstp='git stash pop'
alias gsts='git stash show --text'
alias gsu='git submodule update'
alias gts='git tag -s'
alias gtv='git tag | sort -V'
alias gunignore='git update-index --no-assume-unchanged'
alias gunwip='git log -n 1 | grep -q -c "\-\-wip\-\-" && git reset HEAD~1'
alias gup='git pull --rebase'
alias gupv='git pull --rebase -v'
alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'
alias gwip='git add -A; git rm $(git ls-files --deleted) 2> /dev/null; git commit --no-verify -m "--wip-- [skip ci]"'
