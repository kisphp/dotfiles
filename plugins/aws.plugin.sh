#!/usr/bin/env bash

# https://www.bluematador.com/learn/aws-cli-cheatsheet

# setup aws profile
awsp () { # Set AWS_PROFILE
  export AWS_PROFILE="${1}"
}
awsr () { # Set AWS_REGION
  export AWS_REGION="${1}"
}

# connect to ec2 instances
saws () { # Login into EC2 ubuntu based <ip|hostname>
	ssh ubuntu@"${1}"
}
sawsa () { # Login into EC2 amazon linux based <ip|hostname>
	ssh ec2-user@"${1}"
}

# get aws user id and ARN
alias kwho='aws sts get-caller-identity'
alias whok='kwho'

alias ekscon='aws eks update-kubeconfig --name'
alias eksls='aws eks list-clusters | jq'

awsh () { # Show a list of AWS cli commands
  echo """
  # EC2
  aws ec2 describe-instances
  aws ec2 start-instances --instance-ids i-123123123
  aws ec2 terminate-instances --instance-ids i-123123123

  S3
  aws s3 ls s3://mybucket
  aws s3 rm s3://mybucket --recursive
  aws s3 cp myfolder s3://mybucket/folder --recursive
  aws s3 sync myfolder s3://mybucket/folder --exlude *.tmp
  """
}

ec2ls () { # List EC2 instances
  echo "Getting list of ec2 instances. Please wait..."
# aws ec2 describe-instances | jq -r '.Reservations[].Instances[]|.InstanceId+" "+.InstanceType+" "+(.Tags[] | select(.Key == "Name").Value)'
# aws ec2 describe-instances | jq -r '.Reservations[].Instances[]|.InstanceId+" "+.InstanceType+" "+.State.Name+" "+.InstanceLifecycle' | grep -v terminated
  aws ec2 describe-instances > /tmp/ec2-list.json
  if [[ "$?" != 0 ]];then
    echo "Could not connect to EC2"
    return 1
  fi
  ~/.dotfiles/scripts/ec2_parse.py
  rm /tmp/ec2-list.json
}

# alias r53ls="aws route53 list-hosted-zones | jq -r '.HostedZones[]|.Id+\" => \"+.Name+\" (\"+(.ResourceRecordSetCount|tostring)+\" Records)\"'"

r53ls () { # List route53 zones
    aws route53 list-hosted-zones > /tmp/route53.json
    ~/.dotfiles/scripts/route53_parse.py
    rm /tmp/route53.json
}

s3size () { # Calculate AWS S3 bucket size
  aws s3 ls "s3://${1}" --recursive --human-readable --summarize | tail -2
}


atlon () { # login into ECS Atlantis container
  FARGATE_CONTAINER_ID="${1}"
  echo "$FARGATE_CONTAINER_ID"
  aws ecs execute-command --region us-east-1 --cluster atlantis --task "${FARGATE_CONTAINER_ID}" --container atlantis --command "/bin/bash" --interactive
}

acdk_help () {
  echo "Usage:"
  echo "\tacdk <stack|main> <StackName>"
}
acdk () { # Generate CDK `bin/main.ts` or `lib/NameStack.ts` files
  echo "Generate k3d setup"
  ACTION="${1}"
  TARGET_PATH="${2}"

  if [[ "${TARGET_PATH}" == "" ]]; then
    echo "Please provide the target file path"
    acdk_help
    return 1
  fi

  if [[ "${ACTION}" == "stack" ]]; then
    echo "Generate stack in ${TARGET_PATH}"
    cp ~/.dotfiles/templates/cdk/cdkStack.ts "${TARGET_PATH}.ts"
  elif [[ "${ACTION}" == "main" ]]; then
    echo "Generate main app in ${TARGET_PATH}"
    cp ~/.dotfiles/templates/cdk/cdkMain.ts "${TARGET_PATH}.ts"
  fi
}
