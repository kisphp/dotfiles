#!/usr/bin/env bash

kindinit () { # Generate Makefile and kind config file
    if [[ ! -f "Makefile" ]]; then
        echo "Create Makefile"
cat <<EOF > Makefile
.PHONY: help on off

.DEFAULT_GOAL := help

help: ## Display this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*\$\$' \$(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", \$\$1, \$\$2}'

on: ## Create kind cluster from kind.yaml file
	kind create cluster --config kind.yaml

off: ## Delete kind cluster
	kind delete cluster
EOF
    fi

    if [[ ! -f kind.yaml ]]; then
        echo "Create kind.yaml file"
cat <<EOF > kind.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: k8s-learning
nodes:
  - role: control-plane
  - role: worker
    labels:
      scope: apps
  - role: worker
    labels:
      scope: crons
  - role: worker
EOF
    fi
}

kindnew () { # Create Kind cluster
  if [[ -f kind.yaml ]];then
    kind create cluster --config=kind.yaml "$@"
  else
    kind create cluster "$@"
  fi
}

kindrm () { # Delete Kind cluster
  kind delete cluster "$@"
}
