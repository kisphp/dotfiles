#!/usr/bin/env bash

jenkins_copy () {
  cp ${DOTFILES}/files/Jenkinsfile_${1} ./Jenkinsfile
}

jenkins_phpunit () {
  jenkins_copy phpunit
}

jenkins_codecept () {
  jenkins_copy codeception
}
