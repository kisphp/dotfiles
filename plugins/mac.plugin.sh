#!/usr/bin/env bash

# OS X only:
# "o file.txt" = open file in default app.
# "o http://example.com" = open URL in default browser.
# "o" = open pwd in Finder.
o () { # Open directories in Mac OS
    open ${@:-'.'}
}

stree () { # Open sourcetree
    open -a SourceTree "${1}"
}
