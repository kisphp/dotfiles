#!/usr/bin/env bash

tfmod () { # Generate empty terraform module
    PATH_NAME="."
    if [[ "${1}" != "" ]]; then
      PATH_NAME="${1}"
    fi

    if [[ ! -d "${PATH_NAME}" ]]; then
      mkdir -p "${PATH_NAME}"
      cd "${PATH_NAME}"
    fi

    FILES=("main" "data" "outputs" "variables" "locals")
    echo "Creating recommended terraform module files: "

    for F in "${FILES[@]}"
    do
      printf "\t%s.tf\n" "${F}"
      touch "${F}.tf"
    done
    echo "# Terraform module\n" >> README.md
    echo " "

    if [[ "${1}" != "" ]]; then
      cd -
    fi
}

alias tf='terraform'
alias tfa='terraform apply'
alias tfaa='terraform apply -auto-approve'
alias tfi='terraform init'
alias tfiu='terraform init --upgrade'
alias tfo='terraform output'
alias tfoj='terraform output -json'
alias tfrm='terraform destroy'
alias tfrma='terraform destroy -auto-approve'
alias tfp='terraform plan'
alias tfs='terraform show'
alias tfr='terraform refresh'
alias tfv='terraform validate'
alias tfw='terraform workspace'

# create lock file for all platforms
alias tfpr='echo -e {{linux,darwin}_{amd64,arm64},windows_amd64}"\n" | AWS_PROFILE=default xargs -L1 -I{} -P8 -- bash -c "terraform providers lock -platform={} || true"'

alias tg='terragrunt'
alias tgi='tg init'
alias tgia='tg init && tg apply'
alias tgip='tg init && tg plan'
alias tga='tg apply'
alias tgaa='tg apply -auto-approve'
alias tgp='tg plan'
alias tgo='tg output'
alias tgrm='tg destroy'

tfcostg () { # Generate infracost usage file
  infracost breakdown --sync-usage-file --usage-file infracost-usage.yml --path .
}

tfcost () { # Show infrastructure codes costs
  if [[ -f 'infracost-usage.yml' ]];then
    infracost breakdown --no-cache --usage-file infracost-usage.yml --path .
  else
    infracost breakdown --no-cache --path .
  fi
}

#sec () {
#  terraform output -json | jq -r ."${1}"
#}

tgd () { # Terragrunt graph-dependencies to dot file
  IMG_NAME="${1}"
  if [[ "${IMG_NAME}" == '' ]];then
    IMG_NAME='dependencies'
  fi
  terragrunt graph-dependencies > "${IMG_NAME}".dot
}

tgdi () { # Generate Terragrung dependencies png file from dot file
  IMG_NAME="${1}"
  if [[ "${IMG_NAME}" == '' ]];then
    IMG_NAME='dependencies'
  fi
  cat "${IMG_NAME}".dot | dot -Tpng > ${IMG_NAME}.png
}

#doctf () {
#  CDW=$(pwd)
#  DIRNAME=$(basename $CDW)
#  DOC_HOSTNAME="${DIRNAME}_$(echo $1)"
#
#  docker run --rm \
#  -e TERRAFORM_VERSION="${1}" \
#  -e INFRACOST_API_KEY="${INFRACOST_API_KEY}" \
#  --platform linux/x86_64 \
#  --hostname $(echo $DOC_HOSTNAME | tr '.' '_') \
#  --name $(echo "${DOC_HOSTNAME}" | tr '.' '_' | sed 's/^_//g') \
#  -v ${HOME}/.docker_terraform:/opt/terraform \
#  -v ${HOME}/.aws:/root/.aws \
#  -v ${HOME}/.ssh:/root/.ssh \
#  -v ${CDW}:/project \
#  -w /project \
#  -it registry.gitlab.com/kisphp/ci-registry/terraform:latest bash
#}
#
#alias dtf29='doctf 1.2.9'
#alias dtf33='doctf 1.3.3'

alias dtfu='docker pull registry.gitlab.com/kisphp/ci-registry/terraform:latest'

tfg () { # Terraform generate files
  case "${1}" in
    vpc)
      cat "$DOTFILES/templates/terraform/dvpc.tf" >> data.tf
      ;;
    ami)
      cat "$DOTFILES/templates/terraform/ami.tf" >> ami.tf
      ;;
    aws)
      cat "$DOTFILES/templates/terraform/aws.tf" >> provider.tf
      ;;
    key)
      cat "$DOTFILES/templates/terraform/ec2-key-pair.tf" >> key-pair.tf
      ;;
    ec2)
      cat "$DOTFILES/templates/terraform/ec2-instance.tf" >> ec2-instance.tf
      ;;
    sg)
      cat "$DOTFILES/templates/terraform/security-group.tf" >> security-group.tf
      ;;
    *)
      echo "Usage"
      echo -e "\t ${0} aws  -> provider.tf with aws configuration and data caller identity"
      echo -e "\t ${0} ami  -> ami.tf with aws configuration"
      echo -e "\t ${0} ec2  -> ec2-instance.tf with aws configuration"
      echo -e "\t ${0} key  -> key-pair.tf with aws configuration"
      echo -e "\t ${0} sg   -> security-group.tf with aws configuration"
      echo -e "\t ${0} vpc  -> data.tf to get AWS VPC and Subnets"
      ;;
  esac
}

tfls () { # List versions of terraform installed in /opt/terraform
  echo "List current terraform versions available"
  ls -la /opt/terraform/
}

tfnew () { # Install new terraform version
  VERSION="${1}"

  if [[ -z "${VERSION}" ]];then
    echo "Please provide which version you want o install"
    return 1
  fi

  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
      KERNEL_NAME="linux"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
      KERNEL_NAME="darwin"
  else
      echo "-- Platform not supported: ${OSTYPE}"
      return 1
  fi

  ARCH=$(uname -m)
  if [[ $ARCH == x86_64* ]]; then
    MACHINE="amd64"
  elif [[ $ARCH == arm* ]]; then
    MACHINE="arm64"
  else
    echo "-- Architecture not supported: ${ARCH}"
    return 1
  fi

  if [[ ! -f "/opt/terraform/${VERSION}/terraform" ]];then
    echo "Downloading Terraform: ${VERSION}"
    wget https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && unzip terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && rm terraform_${VERSION}_${KERNEL_NAME}_${MACHINE}.zip \
    && sudo mkdir -p /opt/terraform/${VERSION} \
    && sudo mv terraform /opt/terraform/${VERSION}/ \
    && tflink ${VERSION}
  else
    echo "Terraform version ${VERSION} already exists"
    echo "Run "
    echo "tflink ${VERSION} --> use this version globally"
    echo "alias terraform=\"/opt/terraform/${VERSION}/terraform\" --> use this version locally"
  fi
}

tflink () { # Set terraform version system wide
  VERSION="${1}"

  if [[ -z "${VERSION}" ]];then
    echo "Please provide which version you want o install"
    return 0
  fi

  if [[ ! -f /opt/terraform/${VERSION}/terraform ]]; then
    echo "Terraform version ${VERSION} is not installed."
    echo "Please run 'tfnew ${VERSION}' to install it"
    return 0
  fi

  echo "Setting terraform ${VERSION} as global"
  sudo ln -sf /opt/terraform/${VERSION}/terraform /usr/local/bin/terraform
}
