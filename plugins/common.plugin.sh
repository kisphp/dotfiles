#!/usr/bin/env bash

alias rmme='rm -rf $(pwd) && cd ../'

genpass () { # Generate password
    SIZE="${1}"
    if [[ "${SIZE}" == '' ]]; then
        SIZE="8"
    fi
    head /dev/urandom | tr -dc A-Za-z0-9 | head -c "$SIZE" ; echo ''
}

myip () { # Show my public and private IPs
#    echo "External: $(curl -s https://ifconfig.me)"
    echo "External IPv4: $(curl -s https://api.ipify.org)"
    ip addr show | grep 'autoconf temporary' | awk '{print "External IPv6: " $2}'
    echo ""
    ip addr show | egrep -v 'inet6|172|127' | grep inet | awk '{print "Local: "$2" ("$NF")"}'
}

mcd () { # Create directory and cd into it
    dotfiles_log "Create directory ${1} and get inside it" Common
    if [[ -z "${1}" ]]; then
        errorText "You must specify the directory name"
        return 1
    fi
    if [[ "${1}" == '.' ]];then
        errorText "Cannot create directory with name '.'"
        return 1
    fi
    mkdir -p "${1}" && cd "${1}"
    writeErrorMessage "Could not create directory ${1}"
}

dirsize () { # Calculate the size of a directory
    if [[ -d "${1}" ]]; then
        SIZE=$(ls -alRF $1/ | grep '^-' | awk 'BEGIN {tot=0} {tot=tot+$5} END { print tot }')
        human_size $SIZE
    else
        echo "Directory '${1}' does not exists"
    fi
}

human_size () { # Convert into human readable value
  filesize="$1"

  if [[ -z "$filesize" ]]; then
      echo "Please provide a file size in bytes."
      return 1
  fi

  # Define the units
  units=("B" "KB" "MB" "GB" "TB" "PB" "EB" "ZB" "YB")

  # Determine the appropriate unit
  unit_index=0
  while ((filesize > 1024)); do
      filesize=$((filesize / 1024))
      unit_index=$((unit_index + 1))
  done

  # Print the human-readable size
  echo "$filesize ${units[$unit_index]}"
}
