#!/usr/bin/env bash

k8sclean () { # Delete all k8s pods from all namespaces
    echo "k8s clean"
    kubectl get po --all-namespaces | awk '{if ($4 != "Running") system ("kubectl -n " $1 " delete pods " $2 " --grace-period=0 " " --force ")}';
}

alias mk='minikube'
alias k='kubectl'

mkup () { # Start minikube
    MINIKUBE_DRIVER="docker"
#    MINIKUBE_DRIVER="virtualbox"
    echo "Start minikube with ${KP_MINIKUBE_DISK_SIZE} disk and ${KP_MINIKUBE_MEMORY} memory"
    echo " "
    minikube start \
    --disk-size=${KP_MINIKUBE_DISK_SIZE} \
    --driver=${MINIKUBE_DRIVER} \
    --memory=${KP_MINIKUBE_MEMORY} \
    --alsologtostderr

    minikube addons enable ingress
    minikube addons enable ingress-dns
    minikube addons enable registry
    minikube addons list
    echo " "
    echo "Minikube is available at: $(minikube ip)"
    echo " "
}

# get all k8s resources
alias kala='clear && kubectl get po,svc,deploy,jobs,cronjobs,rs,cm,secret,ing,pv,pvc,no -o wide'
# get pods, services and nodes
alias kal='clear && kubectl get po,svc,no -o wide'

# get a resource
k8s_run () { # Get a k8s resource
  kubectl get -o wide --show-kind "$@"
}
# watch get resource
k8s_run_watch () { # watch every 1second get a k8s resource
  watch -n 1 kubectl get -o wide --show-kind "$@"
}

alias kcj='k8s_run cj' # cronjobs
alias kcjw='k8s_run_watch cj'
alias kcm='k8s_run cm'
alias kcmw='k8s_run_watch cm'
alias kdep='k8s_run deploy'
alias kdepw='k8s_run_watch deploy'
alias kdp='k8s_run deploy'
alias kdpw='k8s_run_watch deploy'
alias kepw='k8s_run_watch ep'
alias kds='k8s_run daemonset'
alias kdsw='k8s_run_watch daemonset'
alias kev='k8s_run ev' # events
alias kep='k8s_run ep' # entrypoint
alias krs='k8s_run replicaset'
alias krsw='k8s_run_watch replicaset'
alias knsw='k8s_run_watch ns'
alias khpa='k8s_run hpa' # horizontalpodautoscalers
alias king='k8s_run ing'
alias kingw='k8s_run_watch ing'
alias kj='k8s_run job'
alias kjob='k8s_run job'
alias kjw='k8s_run_watch job'
alias kjobw='k8s_run_watch job'
alias kpow='k8s_run_watch pod'
alias know='k8s_run_watch no'
alias kno='k8s_run no'
alias kns='k8s_run ns'
alias kpo='k8s_run pod'
alias kpv='k8s_run pv'
alias kpvw='k8s_run_watch pv'
alias kpvc='k8s_run pvc'
alias kpvcw='k8s_run_watch pvc'
alias krrs='k8s_run rs'
alias krrsw='k8s_run_watch rs'
alias krc='k8s_run rc' # replication controller
alias ksec='k8s_run secret'
alias ksecw='k8s_run_watch secret'
alias kss='k8s_run sts' # statefulset
alias ksts='k8s_run sts' # statefulset
alias kssw='k8s_run_watch sts' # statefulset
alias kstsw='k8s_run_watch sts' # statefulset
alias ksa='k8s_run sa' # service account
alias ksc='k8s_run sc' # storageclasses
alias ksv='k8s_run svc'
alias ksvc='k8s_run svc'
alias ksvw='k8s_run_watch svc'
alias ksvcw='k8s_run_watch svc'
alias ktp='kubectl top po'
alias ktn='kubectl top no'
alias kvol='kubectl get pv,pvc -o wide'

alias kd='kubectl describe'
alias kg='kubectl get'
alias ked='kubectl edit'
alias krm='kubectl delete --grace-period=0 --force --wait=false'

# show logs in pod,svc
alias kla='kubectl logs --all-containers'
alias klaf='kubectl logs --all-containers -f'

# change current kubernetes namespace
_kname () {
    NAMESPACE="${1//namespace\//}"
    if [[ "${NAMESPACE}" == '' ]]; then
        NAMESPACE='default'
    fi

    echo "Switching to ${NAMESPACE} namespace"
    kubectl config set-context --current --namespace="${NAMESPACE}"
}

# login with bash into a pod container
kon () { # Get into a K8S pod with bash
  POD="${1//pod\//}"
  shift

  kubectl exec -it "${POD}" -- bash "$@"
}

# login with sh into a pod container
kons () { # Get into a K8S pod with shell
  POD="${1//pod\//}"
  shift

  kubectl exec -it "${POD}" -- sh "$@"
}

alias kname="_kname $@"

k8s_file () {
  TEMPLATE_FILE="${1}"
  APP_NAME="${2}"
  APP_NAMESPACE="${3}"
  TARGET_FILE="${4}"
  if [[ "${TARGET_FILE}" == "" ]]; then
    TARGET_FILE="${TEMPLATE_FILE}.yaml"
  fi
  if [[ "${TARGET_FILE}" != *".yaml"* ]]; then
    TARGET_FILE="${TARGET_FILE}.yaml"
  fi

  echo ""
  echo "Type: ${TEMPLATE_FILE}"
  echo "Name: ${APP_NAME}"
  echo "Namespace: ${APP_NAMESPACE}"
  echo "Target: ${TARGET_FILE}"
  echo ""

  cat "${DOTFILES}/templates/k8s/${TEMPLATE_FILE}.yaml" | \
  sed "s/__K8S_NAME__/${APP_NAME}/g" | \
  sed "s/__K8S_NAMESPACE__/${APP_NAMESPACE}/g" \
  > "${TARGET_FILE}"
}

k8s_split () { # Split multi document kubernetes manifest file
  FILE_PATH="${1}"
  if [[ -z "${1}" ]];then
    echo ""
    echo "Please provide a file path"
    echo ""
  fi
  python3 "${DOTFILES}/scripts/k8s-split.py" "${FILE_PATH}"
}

k8s () { # Generate K8S manifests
  K8S_FILENAME=""
  K8S_NAMESPACE=""

  while getopts "n:f:" flag
  do
#    echo "--${flag}--"
    case "${flag}" in
      f)
        K8S_FILENAME=${OPTARG}
#        echo "found F"
        ;;
      n)
        K8S_NAMESPACE=${OPTARG}
#        echo "found N"
        ;;
    esac
  done
  shift $((OPTIND-1))

  K8S_NAME="${2}"
  if [[ -z "${2}" ]];then
    K8S_NAME="demo"
  fi

  if [[ "${K8S_NAMESPACE}" == "" ]];then
    K8S_NAMESPACE="default"
  fi

  case "${1}" in
    cm|configmap|config-map)
      k8s_file "config-map" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    cj|cron-job)
      k8s_file "cron-job" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    dep|deploy|deployment)
      k8s_file deployment "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    hpa)
      k8s_file "horizontal-pod-autoscaler" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    ing)
      k8s_file "ingress" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    job)
      k8s_file "job" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    pdb)
      k8s_file "pod-disruption-budget" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    pv)
      k8s_file "persistent-volume" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    pvc)
      k8s_file "persistent-volume-claim" "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    sec)
      k8s_file secret "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    sts)
      k8s_file statefulset "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    svc)
      k8s_file service "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    ns)
      k8s_file namespace "${K8S_NAME}" "${K8S_NAMESPACE}" "${K8S_FILENAME}"
      ;;

    split)
      k8s_split "${2}"
      ;;

    ci)
      cp "${DOTFILES}/templates/gitlab-pipeline/.gitlab-ci.yml" .
      ;;

    mr)
      echo "Create gitlab repository MergeRequest template"
      echo "What is gitlab username? "
      read GH_USER
      echo "What is gitlab repository name? "
      read GH_REPO
      mkdir -p .gitlab/merge_request_templates/
      cat "${DOTFILES}/templates/gitlab-pipeline/mr_template.md" \
      | sed "s/__GH_USER__/${GH_USER}/g" \
      | sed "s/__GH_REPO__/${GH_REPO}/g" \
      > .gitlab/merge_request_templates/Default.md
      echo "MergeRequest template done."
      ;;

    *)
      echo "No option available"
      echo ""
      echo "Usage:"
      echo -e "\tk8s [-f filename] [-n namespace] cm    -> config-map"
      echo -e "\tk8s [-f filename] [-n namespace] dep   -> deployment"
      echo -e "\tk8s [-f filename] [-n namespace] sec   -> secret"
      echo -e "\tk8s [-f filename] [-n namespace] svc   -> service"
      echo -e "\tk8s [-f filename] [-n namespace] ing   -> ingress"
      echo -e "\tk8s [-f filename] [-n namespace] cj    -> cron-job"
      echo -e "\tk8s [-f filename] [-n namespace] job   -> job"
      echo -e "\tk8s [-f filename] [-n namespace] pv    -> persistent volume"
      echo -e "\tk8s [-f filename] [-n namespace] pvc   -> persistent volume claim"
      echo -e "\tk8s [-f filename] [-n namespace] hpa   -> horizontal pod autoscaler"
      echo -e "\tk8s [-f filename] ns                   -> namespace"
      echo -e "\tk8s [-f filename] [-n namespace] pdb   -> pod disruption budget"
      echo -e "\tk8s split path/to/manifest.yaml        -> read a single manifest files and split each resource into individual files"
      echo -e "\tk8s ci                                 -> create .gitlab-ci.yaml file for docker build and deploy on k8s (private functionality)"
      echo -e "\tk8s mr                                 -> create merge request template for gitlab repository"
      ;;
  esac
}
