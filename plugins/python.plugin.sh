#!/usr/bin/env bash

alias p2='python2'
alias p3='python3'

# https://stackoverflow.com/questions/2720014/upgrading-all-packages-with-pip
pip2up () { # Upgrade PIP
    $(which pip) freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 sudo pip install -U
}

pipup () { # Upgrade PIP 3
    $(which pip3) freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 sudo pip3 install -U
}

pycli_help () {
  echo " "
  echo "pycli [application-name]"
  echo " "
}

pycli () { # Generate a python cli application with Typer and Poetry
  # Create a poetry and typer cli project
  echo "Creating a Typer CLI application"
  CLI_APP_NAME="${1}"

  if [[ -z "${1}" ]]; then
      errorText "You must specify an application name"
      pycli_help
      return 1
  fi

  echo "Creating application: ${CLI_APP_NAME}"
  poetry new ${CLI_APP_NAME}
  cd ${CLI_APP_NAME}

  penv

  cat <<EOF >> pyproject.toml

[tool.poetry.scripts]
${CLI_APP_NAME} = "${CLI_APP_NAME}.__main__:app"
EOF

  gitci

  poetry add typer pyyaml
  poetry add --dev pylint pytest coverage flake8

  echo "Create Gitlab CI file"
  sed \
      -e "s/__APP_NAME__/${CLI_APP_NAME}/g" \
      ~/.dotfiles/templates/typer_poetry/gitlab-ci-poetry.yaml > .gitlab-ci.yml

  echo "Create test for main script"
  sed \
      -e "s/__APP_NAME__/${CLI_APP_NAME}/g" \
      ~/.dotfiles/templates/typer_poetry/test_main.py > ./tests/test_main.py

  echo "Copy files for linters"
  cp ~/.dotfiles/templates/typer_poetry/.flake8 .
  cp ~/.dotfiles/templates/typer_poetry/.pylintrc .
  cp ~/.dotfiles/templates/typer_poetry/main.py "${CLI_APP_NAME}/__main__.py"

  echo "Test project functionality"
  poetry install
  poetry run -- ${CLI_APP_NAME}
  poetry run -- pytest

  echo "Finish"
  deactivate
  cd -
}

venv () { # Activate python virtual environment
  if [[ -d "venv" ]]; then
    source venv/bin/activate
    return 0
  fi
  if [[ -d ".venv" ]]; then
    source .venv/bin/activate
    return 0
  fi
  echo "\nNo virtual environment in the current directory"
  echo "Run 'penv' command to generate one\n"
}

penv () { # Generate python virtual env and Makefile
    PYTHON_ENV_NAME=".venv"
    $(which python3) -m venv $PYTHON_ENV_NAME
    source $PYTHON_ENV_NAME/bin/activate
    pip install -U pip

    if [[ ! -f "Makefile" ]]; then
cat <<EOF > Makefile
.PHONY: help install save export

.DEFAULT_GOAL := help

help: ## Display this message
	@grep -E '^[a-zA-Z_-]+:.*?## .*\$\$' \$(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", \$\$1, \$\$2}'

install: ## Install pip packages
	pip install -r requirements.txt

save: ## Export installed pip packages list
	pip freeze | sort > requirements.txt

export: ## Export installed pip packages list from poetry
	poetry export --without-hashes --format=requirements.txt > requirements.txt
EOF
    fi
}

pinst () { # Pip install requirements.txt
  $(which pip) install -r requirements.txt
}

pipfr () { # Pip export installed to requirements.txt
  $(which pip) freeze > requirements.txt
}
