#!/usr/bin/env bash

alias trm='terramate'
alias trmg='terramate generate'
alias trmc='terramate create'
alias trml='terramate list'
alias trmf='terramate fmt'
alias trmr='terramate run'

trmi () { # Generate terramate directory project
  cp "${DOTFILES}/templates/terramate/terramate.tm.hcl" terramate.tm.hcl

  if [[ ! -d tm_helpers ]]; then
    mkdir -p tm_helpers/
  fi

#  cp "${DOTFILES}/templates/terramate/generate_provider.tm.hcl" ./tm_helpers/generate_provider.tm.hcl
  cp ${DOTFILES}/templates/terramate/generate_* ./tm_helpers/

  echo "Terramate was configured successfully."
  alias | grep terramate
}
