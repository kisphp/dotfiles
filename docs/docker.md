# `dcimg` or `dcim`

List docker images

```bash
dcimg
```

Update local docker images with latest tag

```shell
dciup
```

List docker images filtered by repository name

```bash
dcimg repository_name
```

> To remove docker images you must pass `-r` argument

> To remove docker dangling images you must pass `-r -e` arguments

List docker processes

```bash
dcpl
```

Stop docker running processes

```bash
dcs
```

Delete docker processes

```bash
dcr
```

List docker networks

```bash
dcnl
```

List docker volumes

```bash
dcv
```

Delete *ALL* docker volumes

```bash
dcv -r
```
